#include <stdio.h>
#include <locale.h>

void insertionSort(int v[], int n) {

    int j, aux;

    for (int i = 1; i < n; i++) {
        aux = v[i];
        for (j = i; (j > 0) && (aux < v[j - 1]); j--)
            v[j] = v[j - 1];

        v[j] = aux;
    }
}

void imprimirVetorDesordenado(int v[], int n) {
    printf("\nVetor n�o ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

void imprimirVetorOrdenado(int v[], int n) {
    insertionSort(v, n);
    printf("\nVetor ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

int main() {
    setlocale(LC_ALL, "Portuguese");

    int vetor1[6] = {2, 1, 4, 3, 6, 5};
    imprimirVetorDesordenado(vetor1, 6);
    imprimirVetorOrdenado(vetor1, 6);

    int vetor2[10] = {2, 1, -4, 3, 6, 5, -8, 7, 10, 9};
    imprimirVetorDesordenado(vetor2, 10);
    imprimirVetorOrdenado(vetor2, 10);

    return 0;
}
